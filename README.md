Modular structure
Clean Architecture
Domain-Driven Design
Test Driven Development
Compilador SWC

Pirâmide de testes
- UI/End to End - .e2e
- Service/Integração - .ispec
- Unit - .spec

-> Limite arquitetural vs Limite Parcial

Objeto de valor
    -> Tipo imutável que possui comportamento


Mocks -> São imitações ou unidades falsas que simulam o comportamento de unidades reais
Stubs -> implementa apenas o mínimo para permitir que o código em teste funcione, ou seja, o stub possui um comportamento previsível de retorno

sobreposição método toString()