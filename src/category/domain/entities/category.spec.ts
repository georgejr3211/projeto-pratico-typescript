import { omit } from "lodash";

import { UniqueEntityId } from "../../../@seedwork/domain/value-objects/unique-entity-id.vo";
import { Category, CategoryProperties } from "./category";

describe("Category Unit Tests", () => {
  test("constructor of category", () => {
    let category: Category = new Category({
      name: "Movie",
    });
    let props = omit(category.props, ["createdAt"]);
    expect(props).toStrictEqual({
      name: "Movie",
      description: null,
      isActive: true,
    });
    expect(category.props.createdAt).toBeInstanceOf(Date);

    category = new Category({
      name: "Movie",
      description: "Movie Category",
      isActive: false,
    });
    let createdAt = new Date();
    expect(category.props).toStrictEqual({
      name: "Movie",
      description: "Movie Category",
      isActive: false,
      createdAt,
    });

    category = new Category({ name: "Movie", description: "Movie Category" });
    expect(category.props).toMatchObject({
      name: "Movie",
      description: "Movie Category",
    });

    category = new Category({ name: "Movie", isActive: true });
    expect(category.props).toMatchObject({
      name: "Movie",
      isActive: true,
    });
  });

  test("id property", () => {
    type CategoryData = { props: CategoryProperties; id?: UniqueEntityId };

    const data: CategoryData[] = [
      { props: { name: "Movie" } },
      { props: { name: "Movie" }, id: null },
      { props: { name: "Movie" }, id: undefined },
      {
        props: { name: "Movie" },
        id: new UniqueEntityId(),
      },
    ];

    data.forEach((item) => {
      const category = new Category(item.props, item.id);
      expect(category.id).not.toBeNull();
      expect(category.uniqueEntityId).toBeInstanceOf(UniqueEntityId);
    });
  });

  test("update method", () => {
    const category = new Category({ name: "Movie" });
    category.update("Movie Category", "Some description");
    expect(category.props).toMatchObject({
      name: "Movie Category",
      description: "Some description",
    });
  });

  test("activate method", () => {
    const category = new Category({ name: "Movie" });
    category.activate();
    expect(category.isActive).toBeTruthy();
  });

  test("deactivate method", () => {
    const category = new Category({ name: "Movie" });
    category.deactivate();
    expect(category.isActive).toBeFalsy();
  });
});
