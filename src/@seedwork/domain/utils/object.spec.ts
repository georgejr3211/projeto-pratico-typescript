import { deepFreeze } from "./object";

describe("Object Utils Unit Tests", () => {
  it("should be a immutable object", () => {
    const str: string = deepFreeze("a");
    expect(typeof str).toBe("string");
  });
});
