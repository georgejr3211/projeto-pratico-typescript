import { InvalidUuidError } from "../../../errors/invalid-uuid.error";
import { UniqueEntityId } from "../unique-entity-id.vo";

describe("Unique Entity Id Unit Tests", () => {
  it("should throw error when uuid is invalid", () => {
    const validateSpy = jest.spyOn(UniqueEntityId.prototype as any, "validate");
    expect(() => new UniqueEntityId("fake id")).toThrow(new InvalidUuidError());
    expect(validateSpy).toBeCalledTimes(1);
  });

  it("should accept a uuid passed in a constructor", () => {
    const validateSpy = jest.spyOn(UniqueEntityId.prototype as any, "validate");
    const uuid: string = "167fb1d0-2fd6-4450-bcd3-ed013f18fc84";
    const vo = new UniqueEntityId(uuid);
    expect(vo.value).toBe(uuid);
    expect(validateSpy).toBeCalledTimes(1);
  });
});
