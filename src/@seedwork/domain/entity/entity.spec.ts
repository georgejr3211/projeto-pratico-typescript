import { UniqueEntityId } from "../value-objects/unique-entity-id.vo";
import { Entity } from "./entity";

class StubEntity extends Entity<{ prop1: string; prop2: string }> {}

describe("Entity Unit Tests", () => {
  it("should set props and id", () => {
    const arrange = { prop1: "prop1", prop2: "prop2" };
    const entity = new StubEntity(arrange);

    expect(entity.props).toStrictEqual(arrange);
    expect(entity.uniqueEntityId).toBeInstanceOf(UniqueEntityId);
    expect(entity.id).not.toBeNull();
  });

  it('should accept a valid uuid as id', () => {
    const arrange = { prop1: "prop1", prop2: "prop2" };
    const id = new UniqueEntityId();
    const entity = new StubEntity(arrange, id);

    expect(entity.props).toStrictEqual(arrange);
    expect(entity.uniqueEntityId).toBeInstanceOf(UniqueEntityId);
    expect(entity.id).toBe(id.value);
  });
});
